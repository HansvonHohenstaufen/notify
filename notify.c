/* See LICENSE file for copyright and license details. */

#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

#include <netinet/in.h>
#include <sys/socket.h>

char*argv0;
#include "util.h"
#include "arg.h"

#include "config.h"

typedef struct Notify Notify;
struct Notify {
	char mess[MAXCHAR];
	struct Notify *next;
};

/* Functions declarations */
static void *clientlisten(void *args);
static void newnotify(char *mess);
static void notification(void);

static struct Notify *listfirst = NULL;
static struct Notify *listlast = NULL;

static void usage(void);

static sem_t mutexlist, mutexnotify;

/* Globals */
static int serversocket;
static int log;

/* Functions implementations */
void *
clientlisten(void *args)
{
	int clientsocket;
	char mess[MAXCHAR];

	while (1)
	{
		clientsocket = accept(serversocket, NULL, NULL);
		if (clientsocket<0)
			die("Client dont connect");
		recv(clientsocket, mess, sizeof(mess), 0);
		newnotify(mess);
		close(clientsocket);
	}

	pthread_exit(NULL);
	return NULL;
}

void
newnotify(char *mess)
{
	int notifyblock;
	struct Notify *last = NULL;

	/* New notify */
	last = (Notify*)malloc(sizeof(Notify));
	sprintf(last->mess, "%s", mess);
	last->next = NULL;

	/* Add to notify list */
	sem_wait(&mutexlist);
	if (listlast==NULL)
		listfirst = last;
	else
		listlast->next = last;
	listlast = last;
	sem_post(&mutexlist);
	sem_post(&mutexnotify);
}

void
notification()
{
	char *mess;
	int notifyblock;
	struct Notify *first;

	FILE *fp;

	while (1)
	{
		sem_wait(&mutexnotify);

		/* Pop notify */
		if (listfirst==NULL)
			continue;
		first = listfirst;

		sem_wait(&mutexlist);
		if (listfirst->next==NULL)
			listfirst = listlast = NULL;
		else
			listfirst = listfirst->next;
		sem_post(&mutexlist);

		/* Syslog */
		if (log)
			syslog(LOG_INFO, "message: %s", first->mess);

		/* Send notify */
		mess = first->mess;
		fp = popen(command, "w");
		if (fp==NULL)
			continue;
		fprintf(fp, "%s", mess);
		pclose(fp);

		/* Delete notify */
		free(first);
	}
}

void
usage()
{
	die(	"usage: notify [OPTION] [MESSAGE]\n"
		"  -l	Enable syslog\n"
		"  -p	Port\n"
		"  -s	Server mode\n"
		"  -v	Show version\n"
	);
}

int
main(int argc, char *argv[])
{
	char mess[MAXCHAR];
	int port;
	int modeserver;
	struct sockaddr_in server;
	pthread_t threadlisten;

	port = 0;
	modeserver = 0;
	log = 0;

	/* Arguments */
	ARGBEGIN {
	case 'l':
		log = 1;
		break;
	case 'p':
		port = atoi(EARGF(usage()));
		break;
	case 's':
		modeserver = 1;
		break;
	case 'v':
		die("notify-"VERSION"\n");
	default:
		usage();
	} ARGEND

	if (!port)
		port = default_port;

	/* Socket init */
	serversocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serversocket<0)
		die("socket don't create.\n");

	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_family = AF_INET;
	server.sin_port = htons(port);

	/* Check client mode */
	if (!modeserver)
	{
		if (!argv[0])
			die("need a message\n");
		sprintf(mess, "%s\n", argv[0]);
		if (connect(serversocket, (struct sockaddr*) &server, sizeof(server))<0)
			die("don't connect to server\n");
		send(serversocket, mess, sizeof(mess), 0);
		close(serversocket);
		return 0;
	}

	/* Connect to server */
	if (bind(serversocket, (struct sockaddr*) &server, sizeof(server))<0)
		die("socket don't bind\n");

	if (listen(serversocket, 5) < 0 )
		die("server error in listen\n");

	/* Start semaphores */
	sem_init(&mutexlist, 0, 1);
	sem_init(&mutexnotify, 0, 1);

	/* Thread -> Listen to clients */
	if (pthread_create(&threadlisten, NULL, clientlisten, NULL))
		die("don't create a thread for listen\n");

	/* Syslog */
	if (log)
	{
		openlog(syslog_ident, LOG_PID, LOG_USER);
		syslog(LOG_INFO, "start daemon");
	}

	notification();

	/* Kill all threads */
	pthread_cancel(threadlisten);

	/* Close socket */
	if (log)
	{
		close(serversocket);
		syslog(LOG_INFO, "end deamon");
	}

	/* Close syslog */
	closelog();

	return 0;
}
