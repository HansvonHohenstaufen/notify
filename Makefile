# notify - simple notification daemon

include config.mk

SRC = notify.c util.c
OBJ = ${SRC:.c=.o}

all: options notify

options:
	@echo notify build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

notify: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f notify ${OBJ} notify-${VERSION}.tar.xz

dist: clean
	mkdir -p notify-${VERSION}
	cp -R ${SRC} config.def.h \
		arg.h util.h \
		Makefile config.mk \
		notify.1 \
		LICENSE README \
		\notify-${VERSION}
	tar -cf notify-${VERSION}.tar notify-${VERSION}
	xz -9 -T0 notify-${VERSION}.tar
	rm -rf notify-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f notify ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/notify
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < notify.1 > ${DESTDIR}${MANPREFIX}/man1/notify.1

uninstall:
	rm -r ${PREFIX}/bin/notify \
		${MANPREFIX}/man1/logo.1

.PHONY: all options clean dist install uninstall
