# notify version
VERSION = 2.0

# Paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# Includes and libs
LIBS = -lpthread

# Flags
CPPFLAGS = -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\"
CFLAGS   = -std=c99 -Os -Wall ${CPPFLAGS}
LDFLAGS  = ${LIBS}

# Compiler
CC = tcc
