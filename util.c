/* See LICENSE file for copyright and license details. */

#include "util.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

void
die(const char *error)
{
	va_list er;

	vfprintf(stderr, error, er);
	va_end(er);
	exit(1);
}
