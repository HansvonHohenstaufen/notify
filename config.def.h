/* See LICENSE file for copyright and license details. */

/* Command */
static const char *command = "dzen2 -p -ta 'l' -h 30 -xs 2";

/* Default port */
static const int default_port = 20000;

/* Max char size */
#define MAXCHAR		1024

/* Syslog */
static const char *syslog_ident = "notify";
